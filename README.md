# Software Studio 2018 Spring Assignment 02 小朋友下樓梯

## 小朋友下樓梯
<img src="example01.png" width="700px" height="500px"></img>

## Goal
1. **Fork the repo ,remove fork relationship and change project visibility to public.**
2. Complete a game "小朋友下樓梯" by Phaser. (JavaScript or TypeScript)
3. Your game should reach the basic requirements.
4. You can download needed materials from some open source webpage to beautify the appearance.
5. Commit to "your" project repository and deploy to Gitlab page.
6. **Report which items you have done and describing other functions or feature in REABME.md.**

## Scoring 
|                                              Item                                              | Score |
|:----------------------------------------------------------------------------------------------:|:-----:|
| A complete game process: start menu => game view => game over => quit or play again            |  20%  |
| Your game should follow the basic rules of  "小朋友下樓梯".                                    |  15%  |
|         All things in your game should have correct physical properties and behaviors.         |  15%  |
| Set up some interesting traps or special mechanisms. .(at least 2 different kinds of platform) |  10%  |
| Add some additional sound effects and UI to enrich your game.                                  |  10%  |
| Store player's name and score in firebase real-time database, and add a leaderboard to your game.        |  10%  |
| Appearance (subjective)                                                                        |  10%  |
| Other creative features in your game (describe on README.md)                                   |  10%  |

## Reminder
* Do not make any change to our root project repository.
* Deploy your web page to Gitlab page, and ensure it works correctly.
    * **Your main page should be named as ```index.html```**
    * **URL should be : https://[studentID].gitlab.io/Assignment_02**
* You should also upload all source code to iLMS.
    * .html or .htm, .css, .js, .ts, etc.
    * source files
* **Deadline: 2018/05/24 23:59 (commit time)**
    * Delay will get 0 point (no reason)
    * Copy will get 0 point
    * "屍體" and 404 is not allowed


firebase URL : https://ss-hw2.firebaseapp.com
some sound bugs may appear while using gitlab page link, strongly recommended to visit through firebase link.

## Report
* Game process
    Boot state -> Load state(a bar would show the loading percentage) -> Menu state(enter the user name and get started)
    -> Score state(show user's score and the leader board)

* Platform styles
    * Standard platform: Player can stand on the platform safely.
    * Nail platform: Player can stand on the platform with -1 health penalty.
    * Trempoline platform: Player would bounce and get a up acceleration.
    * Conveyor platform: Player would get a velocity towards left or right while standing on it.
    * Fake platform: The platform would crack and Player can no long stand on it.
    * Sticky platform: Player would move much slower on it.

* UI
    * The fonts in the game are customize to fit the game characteristic.
    * A dynamic health bar represents Player's health.
    * Animations on the menu page add some spirit to the game.
    * Playing Vivid sound effect while any platfrom effects is triggered.

* Leader board
    * At the score state of the game, it will show the top five scores and usernames. The score would not be recorded if
      user didn't enter his or her username.

* Creative items
    * Coins would appear randomly in the game, Player can earn extra score by eating them.
    * Hearts would appear randomly in the game, Player can heal one health up to the maximum of five.
