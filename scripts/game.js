//Initialize Phaser 
var game = new Phaser.Game(500,800,Phaser.AUTO,'canvas');
game.global = {
    username: 'visitor',
    score: 0
};
//Add states
game.state.add('boot',bootState);
game.state.add('load',loadState);
game.state.add('menu',menuState);
game.state.add('play',playState);
game.state.add('gameover',gameoverState);
//Start game
game.state.start('boot');