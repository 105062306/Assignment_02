var gameoverState ={
    create: function(){
        //stop bgm
        game.bgm.stop();
        game.stage.backgroundColor = '#66aad8';
        var updates = {};
        var fifth_score=0;
        if(game.global.username != 'visitor'){
            var newPostKey = firebase.database().ref().push().key;
            
            updates[newPostKey] = {
                username: game.global.username,
                score: game.global.score
            };
        }
            firebase.database().ref().update(updates).then(function(){
        
            var i=0;
            var leaderboard = firebase.database().ref().orderByChild('score').limitToLast(5);
            leaderboard.on('child_added',function(snapshot) {
                var leaders = game.add.bitmapText(game.width/2-130,600-i*80,'myfont',snapshot.val().username+' : '+snapshot.val().score,45);
                leaders.anchor.setTo(0,0);
                i++;
            });
        });
        //add audio
        this.click = game.add.audio('audio_click');
        
        this.titleLabel = game.add.bitmapText(game.width/2,60,'myfont2','GAME  OVER',40);
        this.titleLabel.anchor.setTo(0.5,0.5);
        this.loadingLabel = game.add.bitmapText(game.width/2,190,'myfont',game.global.username.toUpperCase()+' : '+game.global.score,60);
        this.loadingLabel.anchor.setTo(0.5,0.5);        
        this.first_medal = game.add.image(20,260,'first');
        this.first_medal.width = 70;
        this.first_medal.height = 70;
        this.second_medal = game.add.image(20,340,'second');
        this.second_medal.width = 70;
        this.second_medal.height = 70;
        this.third_medal = game.add.image(20,420,'third');
        this.third_medal.width = 70;
        this.third_medal.height = 70;
        this.forth_medal = game.add.image(20,500,'forth');
        this.forth_medal.width = 70;
        this.forth_medal.height = 70;
        this.fifth_medal = game.add.image(20,580,'fifth');
        this.fifth_medal.width = 70;
        this.fifth_medal.height = 70;

        this.returnLabel = game.add.bitmapText(game.width/2,game.height-70,'myfont','Press Enter to Return',40);
        this.returnLabel.anchor.setTo(0.5,0.5);
            
        this.enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.enterKey.onDown.add(this.start,this);        
    },
    start: function(){  
        this.click.play();
        game.state.start('menu');
    }
    
    
};