var loadState = {
    preload: function(){
        //Add loding label
        loadingLabel = game.add.text(game.width/2,150,'loading......',{font:'30px',fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);
        //Add progess bar
        progressBar = game.add.sprite(game.width/2,200,'progressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);
        //load font
        game.load.bitmapFont('myfont', 'assets/font/font.png', 'assets/font/font.fnt');
        game.load.bitmapFont('myfont2', 'assets/font2/font.png', 'assets/font2/font.fnt');
        //load images
        game.load.image('bg','./assets/bg.png');
        game.load.image('ceiling','./assets/ceiling.png');
        game.load.spritesheet('conveyor_right','./assets/conveyor_right.png', 96, 16);
        game.load.spritesheet('conveyor_left','./assets/conveyor_left.png', 96, 16);
        game.load.spritesheet('fake', './assets/fake.png', 96, 36);
        game.load.image('nails','./assets/nails.png');
        game.load.image('normal','./assets/normal.png');
        game.load.image('sticky','./assets/sticky.png');
        game.load.spritesheet('trampoline','./assets/trampoline.png', 96, 22);
        game.load.spritesheet('coin','./assets/coin.png', 27,28);
        game.load.image('heart','./assets/heart.png');
        game.load.image('wall','./assets/wall.png');
        game.load.image('stone','./assets/stone.jpg');
        game.load.spritesheet('player','./assets/player.png', 32, 32);
        game.load.spritesheet('health_bar','./assets/Health_Bar.png', 204, 30);

        game.load.image('first','./assets/first.png');
        game.load.image('second','./assets/second.png');
        game.load.image('third','./assets/third.png');
        game.load.image('forth','./assets/forth.png');
        game.load.image('fifth','./assets/fifth.png');
        //load audio
        game.load.audio('audio_click','./assets/audio/click.mp3');
        game.load.audio('audio_coin','./assets/audio/coin.mp3');
        game.load.audio('audio_hurt','./assets/audio/hurt.mp3');
        game.load.audio('audio_break','./assets/audio/break.mp3');
        game.load.audio('audio_bounce','./assets/audio/bounce.mp3');
        game.load.audio('audio_motor','./assets/audio/motor.mp3');
        game.load.audio('audio_sticky','./assets/audio/sticky.mp3');
        game.load.audio('audio_heal','./assets/audio/heal.mp3');
        game.load.audio('bgm','./assets/audio/bgm.mp3');

    },
    create: function(){
        game.state.start('menu');
    }    
};