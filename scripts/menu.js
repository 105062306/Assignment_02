var menuState = {
    create: function(){
        game.stage.backgroundColor = '#000000';
        //ADD background
        game.add.image(0,0,'bg');
        //add audio
        this.click = game.add.audio('audio_click');
        //game.stage.backgroundColor = 'rgb(189, 98, 192)';
        //display name of the game
        this.titleLabel = game.add.bitmapText(game.width/2,150,'myfont2','NS-SHAFT',50);
        this.titleLabel.anchor.setTo(0.5,0.5);
        //show score
       // this.scoreLabel = game.add.text(game.width/2,game.height/2,'score'+game.global.score,{font:'25px'});
        //this.scoreLabel.anchor.setTo(0.5,0.5);
        //show how to start
        this.startLabel = game.add.bitmapText(game.width/2,game.height-50,'myfont','Press Enter to Start',40);
        this.startLabel.anchor.setTo(0.5,0.5);
        //show textbar
        this.textbar = document.getElementById('textbar');
        textbar.style.display = 'block';
        //add animation
        this.player_r = game.add.sprite(game.width, 30, 'player');
        this.player_r.anchor.setTo(1,0);
        this.player_r.scale.setTo(2.0,2.0);
        this.player_r.animations.add('left', [0, 1, 2, 3], 8,true);
        this.player_r.animations.play('left');

        this.player_l = game.add.sprite(0, 30, 'player');
        this.player_l.anchor.setTo(0,0);
        this.player_l.scale.setTo(2.0,2.0);
        this.player_l.animations.add('right', [9, 10, 11, 12], 8,true);
        this.player_l.animations.play('right');

        this.player_m = game.add.sprite(game.width/2, 30, 'player');
        this.player_m.scale.setTo(2.0,2.0);
        this.player_m.anchor.setTo(0.5,0);
        this.player_m.animations.add('fly', [36, 37, 38, 39], 8,true);
        this.player_m.animations.play('fly');
        //when press enter, game start
        this.enterKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        this.enterKey.onDown.add(this.start,this);        
    },
    start: function(){        
        game.global.username = textbar.value || 'visitor';
        textbar.value = '';
        textbar.style.display = 'none';
        game.global.score = 0;
        this.click.play();
        //game start
        game.state.start('play');
    }
   
};