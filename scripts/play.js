var playState = {
    create: function(){
        //add audio
        this.audio_coin = game.add.audio('audio_coin');
        this.audio_heal = game.add.audio('audio_heal');
        this.audio_hurt = game.add.audio('audio_hurt');
        this.audio_break = game.add.audio('audio_break');
        this.audio_bounce = game.add.audio('audio_bounce');
        this.audio_motor = game.add.audio('audio_motor');
        this.audio_sticky = game.add.audio('audio_sticky');
        game.bgm = game.add.audio('bgm');
        //play bgm
        game.bgm.play();
        //create walls and ceiling
        this.left_wall = game.add.sprite(0,0,'wall');
        this.left_wall.height = game.height-140;
        this.left_wall.anchor.setTo(0,0);
        game.physics.arcade.enable(this.left_wall);
        this.left_wall.body.immovable = true;

        this.right_wall = game.add.sprite(game.width,0,'wall');
        this.right_wall.height = game.height-140;
        this.right_wall.anchor.setTo(1,0);
        game.physics.arcade.enable(this.right_wall);
        this.right_wall.body.immovable = true;

        this.ceiling = game.add.sprite(this.left_wall.width,0,'ceiling');
        this.ceiling.width = game.width-this.right_wall.width*2;
        this.ceiling.anchor.setTo(0,0);
        game.physics.arcade.enable(this.ceiling);
        this.ceiling.body.immovable = true;
        //create player
        this.player = game.add.sprite(200, 50, 'player');
        game.physics.arcade.enable(this.player);
        this.player.anchor.setTo(0.5,1);
        this.player.body.gravity.y = 400;
        this.player.animations.add('left', [0, 1, 2, 3], 8);
        this.player.animations.add('right', [9, 10, 11, 12], 8);
        this.player.animations.add('flyleft', [18, 19, 20, 21], 12);
        this.player.animations.add('flyright', [27, 28, 29, 30], 12);
        this.player.animations.add('fly', [36, 37, 38, 39], 12);
        this.player.health = 5;
        this.player.immortal_time = 0;
        this.player.land_on = undefined;
        //add status board
        this.game_status = game.add.image(0,game.height-150,'stone');
        this.game_status.width = game.width+10;
        this.game_status.height = 160;
        this.player_name = game.add.bitmapText(game.width-10,game.height-50,'myfont','player : '+game.global.username,30);
        this.player_name.anchor.setTo(1,0);
        this.health_tag = game.add.bitmapText(10,game.height-120,'myfont2','HEALTH ',18);
        this.health_bar = game.add.sprite(150,game.height-115,'health_bar');
        this.health_bar.width = 350;
        this.health_bar.frame = 0;
        this.score_board = game.add.bitmapText(10,game.height-60,'myfont2','SCORE',18); 
        // Bricks group
        this.conveyor_right_g = game.add.group();
        this.conveyor_right_g.enableBody = true;  

        this.conveyor_left_g = game.add.group();
        this.conveyor_left_g.enableBody = true;  

        this.fake_g = game.add.group();
        this.fake_g.enableBody = true;  

        this.normal_g = game.add.group();
        this.normal_g.enableBody = true;   
        
        this.nails_g = game.add.group();
        this.nails_g.enableBody = true;  

        this.trampoline_g = game.add.group();
        this.trampoline_g.enableBody = true;  

        this.sticky_g = game.add.group();
        this.sticky_g.enableBody = true;  

        this.coin_g = game.add.group();
        this.coin_g.enableBody = true; 
        
        this.heart_g = game.add.group();
        this.heart_g.enableBody = true;

        //create score and time flag and brick type
        this.score = 0;
        this.time_flag = 0;
        this.brick_type = 0;
        
        //create keyboard cursor
        this.cursor = game.input.keyboard.createCursorKeys();
    },
    update: function(){
        game.physics.arcade.collide(this.player, this.left_wall);
        game.physics.arcade.collide(this.player, this.right_wall);
        game.physics.arcade.collide(this.player, this.ceiling,this.touch_ceiling,null,this);
        game.physics.arcade.collide(this.player, this.normal_g);
        game.physics.arcade.collide(this.player, this.conveyor_left_g,this.touch_conveyor_left_g,null,this);
        game.physics.arcade.collide(this.player, this.conveyor_right_g,this.touch_conveyor_right_g,null,this);
        game.physics.arcade.collide(this.player, this.trampoline_g,this.touch_trampoline_g,null,this);
        game.physics.arcade.collide(this.player, this.nails_g,this.touch_nails_g,null,this);
        game.physics.arcade.collide(this.player, this.fake_g,this.touch_fake_g,null,this);
        game.physics.arcade.collide(this.player, this.sticky_g,this.touch_sticky_g,null,this);
        game.physics.arcade.collide(this.player, this.coin_g,this.touch_coin_g,null,this);
        game.physics.arcade.collide(this.player, this.heart_g,this.touch_heart_g,null,this);

        this.player_move();
        this.checkhealth();
        this.create_bricks();
        this.bricks_move();
        this.score_board.setText('SCORE  '+game.global.score);
        this.health_bar.frame = 5-this.player.health;
    },
    player_move: function(){
        if(this.cursor.left.isDown){
            this.player.body.velocity.x = -250;
            if(this.player.body.velocity.y!=0)
            this.player.animations.play('flyleft');
            else    
            this.player.animations.play('left');
        }
        else if(this.cursor.right.isDown){
            this.player.body.velocity.x = 250;
            if(this.player.body.velocity.y!=0)
            this.player.animations.play('flyright');
            else    
            this.player.animations.play('right');
        }
        else{
            this.player.body.velocity.x = 0;
            if(this.player.body.velocity.y!=0)
                this.player.animations.play('fly');
            else
                this.player.frame = 8;
        }
        
    },
    create_bricks: function(){
        if(this.time_flag + 600 < game.time.now){
            game.global.score += 3;
            this.time_flag = game.time.now;
            this.score += 1;
            var brick;
            this.brick_type = game.rnd.integerInRange(0, 100);
            var x_pos = game.rnd.integerInRange(this.left_wall.width + 50, game.width - this.right_wall.width - 50);

            if(this.brick_type < 30){ //normal brick
                brick = game.add.sprite(x_pos,game.height-160,'normal',0,this.normal_g);
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            }
            else if(this.brick_type < 40){ //sticky brick
                brick = game.add.sprite(x_pos,game.height-160,'sticky',0,this.sticky_g);
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            }
            else if(this.brick_type < 50){ //coin
                brick = game.add.sprite(x_pos,game.height-160,'coin',0,this.coin_g);
                brick.animations.add('effect', [9,8,7,6,5,4,3,2,1,0],16,true);
                brick.animations.play('effect');
                
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            }
            else if(this.brick_type < 55){ //heart
                brick = game.add.sprite(x_pos,game.height-160,'heart',0,this.heart_g);
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            }
            else if(this.brick_type < 60){ //trampoline brick
                brick = game.add.sprite(x_pos,game.height-160,'trampoline',0,this.trampoline_g);
                brick.animations.add('effect', [4, 5, 4, 3, 2, 1, 0, 1, 2, 3], 16);
                brick.frame = 3;
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            } 
            else if(this.brick_type < 70){ // conveyor_right brick
                brick = game.add.sprite(x_pos,game.height-160,'conveyor_right',0,this.conveyor_right_g);
                brick.animations.add('effect', [0, 1, 2, 3], 16, true);
                brick.animations.play('effect');
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            } 
            else if(this.brick_type < 80){ // conveyor_left brick
                brick = game.add.sprite(x_pos,game.height-160,'conveyor_left',0,this.conveyor_left_g);
                brick.animations.add('effect', [0, 1, 2, 3], 16, true);
                brick.animations.play('effect');
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            } 
            else if(this.brick_type < 90){ // fake brick
                brick = game.add.sprite(x_pos,game.height-160,'fake',0,this.fake_g);
                brick.animations.add('effect', [0, 1, 2, 3, 4, 5, 0], 16);
                brick.width = 100;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);  
                brick.body.immovable = true;              
            } 
            else if(this.brick_type < 100){ // nails brick
                brick = game.add.sprite(x_pos,game.height-160,'nails',0,this.nails_g);
                brick.width = 100;
                brick.body.offset.y = 15;
                game.physics.arcade.enable(brick);
                brick.enableBody = true;  
                brick.anchor.setTo(0.5,0.5);
                brick.body.immovable = true;
            } 
        }
    },
    bricks_move: function(){
        this.conveyor_right_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.conveyor_left_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.sticky_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.coin_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.heart_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });


        this.fake_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.normal_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });
        
        this.nails_g.forEach(element => {            
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });

        this.trampoline_g.forEach(element => {
            element.body.position.y -= 2;
            if( element.body.position.y <= -20)
                element.kill();
        });
    },
    touch_ceiling: function(player,ceiling){
        /*if(player.body.y<0){
            if( player.body.velocity.y<0)
                player.body.velocity.y = 0;                    
            if(game.time.now > this.player.immortal_time){
                player.health -= 1;
                player.immortal_time = game.time.now +2000;
            }
        }*/
        this.audio_hurt.play();
        game.camera.flash(0xff0000, 100);
        game.state.start('gameover');
    },
    touch_sticky_g: function(player,brick){
        if(brick.body.touching.up){
            this.audio_sticky.play();
            if(player.body.velocity.x > 0)
                player.body.x -= 3;
            else if(player.body.velocity.x < 0)
                player.body.x += 3;
        }
    },
    touch_coin_g: function(player,brick){
        this.audio_coin.play();
        game.global.score += 50;
        brick.kill();
    },
    touch_heart_g: function(player,brick){        
        if(player.health<5){
            this.audio_heal.play(); 
            player.health += 1;
        }
           
        brick.kill();
    },
    touch_conveyor_left_g: function(player,brick){
        if(brick.body.touching.up){
            this.audio_motor.play();
            player.body.x -=3;
        }
    },
    touch_conveyor_right_g: function(player,brick){
        if(brick.body.touching.up){
            this.audio_motor.play();
            player.body.x +=3;
        }
    },
    touch_trampoline_g: function(player,brick){
        if(brick.body.touching.up){
            this.audio_bounce.play();
            brick.animations.play('effect');
            player.body.velocity.y -=250;
        }
    },  
    touch_nails_g: function(player,brick){
        if(brick.body.touching.up && player.land_on != brick){
            this.audio_hurt.play();
            game.camera.flash(0xff0000, 100);
            player.land_on = brick;
            brick.animations.play('effect');
            player.health -= 1;
        }
    },  
    touch_fake_g: function(player,brick){
        if(brick.body.touching.up && player.land_on != brick){
            this.audio_break.play();
            player.land_on =brick;            
            setTimeout(function() {
                brick.body.checkCollision.up = false;
            }, 100);
            brick.animations.play('effect');
        }
    },  
    checkhealth: function(){
        if(this.player.health==0 || this.player.body.y > game.height-150){
            this.player.kill();
            game.state.start('gameover');
        }
            
    },

};